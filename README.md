# uBlocklist Block List

> **WARNING**: This is not for public!

uBlock Origin Filtering stuffs.

To add:
1. Go to uBlock Origin, then go to Filter List.

2. Add the following urls to the "Import" section:
    ```
    https://gitlab.com/chez14/ublocklist-block-list/-/raw/main/youtube-stuff.txt
    https://gitlab.com/chez14/ublocklist-block-list/-/raw/main/fandom.txt
    ```